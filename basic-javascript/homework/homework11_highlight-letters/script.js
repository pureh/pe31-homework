const arrBtns = Array.from(document.querySelectorAll('.btn-wrapper .btn'));
const arrText = arrBtns.map((letter) => letter.textContent.toUpperCase());

document.addEventListener('keydown', function(e) {
	// find index of pressed button in array of buttons;
	const pressedBtnIdx = arrBtns.findIndex((textItem) => e.key.toUpperCase() === textItem.textContent.toUpperCase());

	arrBtns.forEach((el, idx) => {
		el.style.backgroundColor = '#000000';
		if (idx === pressedBtnIdx) {
			el.style.backgroundColor = 'red';
		}
	});
});
