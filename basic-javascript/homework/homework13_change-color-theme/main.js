let button = document.querySelector('#change-theme');
let hrefNew = './css/dark-style.css';
let hrefOld = './css/style.css';
let hrefCheck = document.getElementById('style').getAttribute('href');

button.addEventListener('click', function() {
	if (hrefCheck == hrefOld) {
		document.getElementById('style').href = hrefNew;
		hrefCheck = './css/dark-style.css';
		localStorage.setItem('styleSaved', hrefCheck);
		console.log(hrefCheck);
	} else if (hrefCheck == hrefNew) {
		document.getElementById('style').href = hrefOld;
		hrefCheck = './css/style.css';
		localStorage.setItem('styleSaved', hrefCheck);
		console.log(hrefCheck);
	}
});

let themeStorage = localStorage.getItem('styleSaved');
console.log(themeStorage);