const student = {
	fname: '',
	lname: '',
	tabel: {},
};
student.fname = prompt('Your first name, student', 'John');
student.lname = prompt('Your last name, student', 'Doe');

while (true) {
	let subject = prompt('Enter subject name:');
	if (subject === null) {
		break;
	}

	let mark = prompt('Enter subject mark:');
	while (mark === '' || mark === null || isNaN(+mark)) {
		mark = prompt('Please, enter correct mark:');
	}
	student.tabel[subject] = +mark;
}

const checkMark = function(obj) {
	let badMark = 0;
	for (let key in obj) {
		if (obj[key] < 4) badMark++;
	}
	if (badMark == 0) return console.log('Студент переведен на следующий курс');
	return console.log(`You in the army now, ${student.fname} ${student.lname}!`);
};

const averageScore = function(obj) {
	let sum = 0;
	let items = 0;
	for (let key in obj) {
		items++;
		sum += obj[key];
	}
	if (sum / items > 7) return console.log('Студенту назначена стипендия');
	return console.log('Студенту не назначена стипендия :(');
};

checkMark(student.tabel);
averageScore(student.tabel);
