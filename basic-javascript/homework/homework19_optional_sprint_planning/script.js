const teamArr = [ 10, 10, 10, 10, 10 ];
const tasksList = [ 100 ];
const presentDate = new Date();
const deadlineDate = new Date('03/24/2021');

const calcDeadline = (teamPoints, tasksPoints, deadline) => {
	let peoplePoints = teamPoints.reduce((a, b) => a + b, 0);
	let backlogPoints = tasksPoints.reduce((a, b) => a + b, 0);
	let remainder = backlogPoints;

	for (let i = presentDate.getDate(); i < deadline.getDate(); i++) {
		presentDate.setDate(presentDate.getDate() + 1);
		if (presentDate.getDay() === 6 || presentDate.getDay() === 0) {
			presentDate.setDate(presentDate.getDate() + 1);
			continue;
		}
		remainder -= peoplePoints;
		if (remainder <= 0) {
			console.log(`Все задачи будут успешно выполнены за ${deadline.getDate() - i} дней до наступления дедлайна!`);
			return true;
		}
	}
	console.log(`Команде разработчиков придется потратить дополнительно ${remainder / peoplePoints * 8} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
};

calcDeadline(teamArr, tasksList, deadlineDate);
