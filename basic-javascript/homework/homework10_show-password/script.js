const ficon = document.querySelector('#ficon');
const sicon = document.querySelector('#sicon');

function showPasswordInput() {
	const password = document.getElementById('input');
	if (password.type === 'text') {
		password.type = 'password';
	} else {
		password.type = 'text';
	}
	ficon.classList.toggle('fa-eye-slash');
	ficon.classList.toggle('fa-eye');
}
function showPasswordConfirm() {
	const password = document.getElementById('confirm');
	if (password.type === 'text') {
		password.type = 'password';
	} else {
		password.type = 'text';
	}
	sicon.classList.toggle('fa-eye-slash');
	sicon.classList.toggle('fa-eye');
}

ficon.addEventListener('click', showPasswordInput);
sicon.addEventListener('click', showPasswordConfirm);

document.querySelector('.btn').addEventListener('click', () => {
	const input = document.getElementById('input');
	const confirm = document.getElementById('confirm');
	const wrongPass = document.querySelector('.wrong-pass');
	if (input.value === confirm.value) {
		wrongPass.innerText = '';
		setTimeout(function() {
			alert("You're Welcome!");
		}, 100);
	} else {
		wrongPass.innerText = 'Passwords must match. Try again';
		wrongPass.style.color = 'red';
		wrongPass.style.marginBottom = '24px';
	}
});
