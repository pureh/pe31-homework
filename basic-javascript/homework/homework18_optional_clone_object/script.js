function copyObject(value) {
	let result = {};
	for (let key in value) {
		if (typeof value[key] !== 'object') {
			result[key] = value[key];
		} else {
			const innerOnject = value[key];
			result[key] = copyObject(innerOnject);
		}
	}
	return result;
}

const testObj = {
	firstName: 'John',
	lastName: 'Smith',
	isAlive: true,
	age: 27,
	address: {
		streetAddress: '21 2nd Street',
		city: 'New York',
		state: 'NY',
		postalCode: '10021-3100',
	},
	phoneNumber: {
		type: 'home',
		number: '212 555-1234',
	},
	children: [],
	spouse: null,
};

console.log(testObj);
const copiedTestObject = copyObject(testObj);
console.log(copiedTestObject);
