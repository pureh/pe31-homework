const books = [
	{
		author: 'Скотт Бэккер',
		name: 'Тьма, что приходит прежде',
		price: 70,
	},
	{
		author: 'Скотт Бэккер',
		name: 'Воин-пророк',
	},
	{
		name: 'Тысячекратная мысль',
		price: 70,
	},
	{
		author: 'Скотт Бэккер',
		name: 'Нечестивый Консульт',
		price: 70,
	},
	{
		author: 'Дарья Донцова',
		name: 'Детектив на диете',
		price: 40,
	},
	{
		author: 'Дарья Донцова',
		name: 'Дед Снегур и Морозочка',
	},
];

const divRoot = document.createElement('div');
divRoot.id = 'root';
document.body.append(divRoot);

const ul = document.createElement('ul');
divRoot.append(ul);

function validateBook(item, index) {
	if (!item.author) {
		throw new SyntaxError(`No autor at arr element at id [${index}]]`);
	}
	if (!item.name) {
		throw new SyntaxError(`No name at arr element at id [${index}]]`);
	}
	if (!item.price) {
		throw new SyntaxError(`No price at arr element at id [${index}]]`);
	}
}

books.forEach((item, index) => {
	const li = document.createElement('li');

	try {
		validateBook(item, index);
		li.innerHTML = `${item.author}, ${item.name} - ${item.price} гривень`;
		ul.append(li);
	} catch (error) {
		console.error(error.message);
	}
});
