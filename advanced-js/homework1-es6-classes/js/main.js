/* Обьясните своими словами, как вы понимаете, как работает прототипное
наследование в Javascript

Прототипное наследование работает таким образом, что благодаря специальному синтаксису через использование Class, у нас есть возможность легче создавать новые объекты. Используя Class можно создать такой себе "шаблон" для создание последующих объектов, при этом можно указать нужные параметры которые нужно унаследовать от другого объекта. */

class Employee {
	constructor({ name, age, salary }) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
	set name(name) {
		this._name = name;
	}
	get name() {
		return this._name;
	}
	set age(age) {
		this._age = age;
	}
	get age() {
		return this._age;
	}
	set salary(salary) {
		this._salary = salary;
	}
	get salary() {
		return this._salary;
	}
}

class Programmer extends Employee {
	constructor({ name, age, salary, lang }) {
		super({ name, age, salary });
		this._lang = lang;
	}
	set salary(salary) {
		this._salary = salary;
	}
	get salary() {
		return this._salary * 3;
	}
}

const progr1 = new Programmer({
	name: 'test',
	age: '20',
	salary: 9000,
	lang: [ 'JS', 'PHP' ],
});
console.log(progr1);

const progr2 = new Programmer({
	name: 'test1',
	age: '25',
	salary: 2000,
	lang: [ 'JS', 'PHP', 'Go' ],
});
console.log(progr2);

const progr3 = new Programmer({
	name: 'test2',
	age: '19',
	salary: 20,
	lang: [ 'Ruby' ],
});
console.log(progr3);
