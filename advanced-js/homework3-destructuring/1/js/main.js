const clients1 = [ 'Гилберт', 'Сальваторе', 'Пирс', 'Соммерс', 'Форбс', 'Донован', 'Беннет' ];
const clients2 = [ 'Пирс', 'Зальцман', 'Сальваторе', 'Майклсон' ];

let array = [ ...clients1, ...clients2 ];

const uniqueArray = new Set(array);
const result = [ ...uniqueArray ];

console.log(result);
