const btn = document.createElement("button")
btn.innerHTML = "Вычислить по IP"
document.body.append(btn)

btn.onclick = async function() {
    const ip = await fetch("https://api.ipify.org?format=json");
    const clearIp = await ip.json();
    const geo = await fetch(`http://ip-api.com/json/${clearIp.ip}?lang=ru&fields=continent,country,region,city,district`);
    const clearGeo = await geo.json();
    console.log(clearGeo.continent)
    const continent = document.createElement("p")
    continent.innerHTML = clearGeo.continent
    const country = document.createElement("p")
    country.innerHTML = clearGeo.country
    const region = document.createElement("p")
    region.innerHTML = clearGeo.region
    const city = document.createElement("p")
    city.innerHTML = clearGeo.city
    const district = document.createElement("p")
    district.innerHTML = clearGeo.district
    document.body.append(continent, country, region, city, district)
}

