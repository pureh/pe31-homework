const del = require("del");

const delDist = () => {
  return del(["./dist/*"]);
};

exports.del = delDist;
