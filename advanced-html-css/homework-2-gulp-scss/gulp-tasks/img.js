const { src, dest } = require("gulp");
const imageMin = require("gulp-imagemin");

const img = () =>
  src("./src/img/**/*")
    .pipe(imageMin({ progressive: true }))
    .pipe(dest("./dist/img/"));

exports.img = img;
